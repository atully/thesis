\chapter{Introduction}
\label{chap:intro}

\graphicspath{{intro/figs/}}

%% Restart the numbering to make sure that this is definitely page #1!
\pagenumbering{arabic}

%\chapterquote{In the beginning there was nothing, which exploded.}%
%{Terry Pratchett}%: Source
\chapterquote{Not only is the Universe stranger than we think, it is stranger than we can think.}
{Werner Heisenberg}

The rather unfairly named ``Standard Model of particle physics'' (SM) is really quite remarkable. It describes the basic building blocks from which everything in the universe is made (the ``fundamental particles'') and their interactions under the electromagnetic, weak and strong forces. The SM contains twelve matter particles of spin-1/2, known as fermions, four force particles of spin-1, known as gauge bosons, and the Higgs boson, which is related to the mechanism that grants particles mass. The fermions are made up of six flavours of quarks $(u,d,c,s,t,b)$, which interact under all three forces, and leptons $(\en, \neue, \mun, \neum,\taum,\neut)$, which do not interact under the strong force. The photon mediates the electromagnetic interaction, the $\W$ and \Z bosons the weak interaction and the gluon the strong interaction. Each of the particles in the SM has an antiparticle counterpart, which, aside from charge, is otherwise identical.\footnote{Some particles, such as the photon, are their own antiparticle.} The particle zoo is summarised in table~\ref{tab:particlezoo}.

\begin{table}[tb]
\centering
\resizebox{\textwidth}{!}{
  \begin{tabular}{llcccc}
  \toprule
   Particle type & Name & Symbol & Mass$~(\mevcc)$ & Spin & Charge$~(e)$ \\
  \midrule
  \multirow{6}{*}{Quarks} & Up     & $\uquark$ & $2.16^{+0.49}_{-0.26}$ & $\frac{1}{2}$ & $+\frac{2}{3}$\\
  & Down & $\dquark$ & $4.67^{+0.48}_{-0.17}$ & $\frac{1}{2}$ & $-\frac{1}{3}$\\
  & Charm   & $\cquark$ & $1,270\pm 20$ & $\frac{1}{2}$ & $+\frac{2}{3}$\\
  & Strange & $\squark$ & $93^{+11}_{-5}$ & $\frac{1}{2}$ & $-\frac{1}{3}$\\
  & Top      & $\tquark$ & $172,900\pm 400$ & $\frac{1}{2}$ & $+\frac{2}{3}$\\
  & Bottom & $\bquark$ & $4,180^{+30}_{-20}$ & $\frac{1}{2}$ & $-\frac{1}{3}$\\
  \midrule
  \multirow{6}{*}{Leptons} & Electron              & $\en$ & $0.5109989461\pm0.0000000031$ & $\frac{1}{2}$ & $-1$\\
  & Electron neutrino & $\neue$ & $<0.000002$ & $\frac{1}{2}$ & $0$\\
  & Muon              & $\mun$ & $105.6583745\pm 0.0000024$ & $\frac{1}{2}$ & $-1$\\
  & Muon neutrino & $\neum$ & $<0.19$ & $\frac{1}{2}$ & $0$\\
  & Tau              & $\taum$ & $1776.86\pm 0.12$ & $\frac{1}{2}$ & $-1$\\
  & Tau neutrino & $\neut$ & $<18.2$ & $\frac{1}{2}$ & $0$\\
  \midrule
  \multirow{5}{*}{Bosons} & Photon & $\gamma$ & 0 & 1 & 0\\
  & \W boson & $\Wpm$ & $80,379\pm 12$ & 1 & $\pm1$\\
  & \Z boson & $\Z$ & $91,187.6\pm 2.1$ & 1 & 0\\
  & Gluon & $g$ & 0 & 1 & 0\\
  & Higgs boson & \H & $125,100\pm 140$ & 0 & 0\\
  \bottomrule
  \end{tabular}
  }
    \caption{The mass, spin and electric charge of the particles in the SM~\cite{PDG2018}.}
	\label{tab:particlezoo}
\end{table}
\vspace{-0.4cm}

The formation of the SM in the latter half of the 20th century was the combined brain power of a myriad of physicists across the globe, 55 of whom were awarded the Nobel Prize in Physics. Every particle the SM has predicted to exist has been discovered, culminating with the discovery of the Higgs boson in 2012~\cite{Aad:2012tfa,Chatrchyan:2012xdj} after a 40 year-long search effort. The precision to which some of its predictions have been verified is nothing short of astonishing. For example, the prediction of the anomalous magnetic moment of the electron agrees with the experimental measurement to about one part in a trillion~\cite{Hanneke:2010au}.  With the operation of the Large Hadron Collider (\lhc), the SM has been placed under an ever more powerful microscope. A task force of thousands of physicists at the \lhc experiments have performed a comprehensive suite of measurements probing various corners of the theory, but so far, it has proved impregnable.

Although it may seem like the career prospects of a budding particle physicist look rather bleak, despite its success, the SM leaves many important phenomena unexplained. Of the four fundamental forces (electromagnetic, weak, strong and gravity), it incorporates all but gravity. Although this isn't a problem for the very light particles collided at the \lhc, there is a disconnect between the description of physics on very small, atomic scales (quantum field theory) and very large, astronomical scales (Einstein's theory of General Relativity).

Of the total energy density of the universe, the particles in the SM only make up a mere $\sim 5\%$.~\cite{Aghanim:2018eyx}. Of the rest, $\sim 25\%$ is ``dark matter'', a strange form of matter that interacts gravitationally but is invisible when viewed through a telescope, and $\sim 70\%$ is ``dark energy'', a mysterious form of energy with anti-gravitational properties, thought to be responsible for the accelerating expansion of the universe~\cite{Peebles:2002gy}. The SM does not contain any dark matter candidates, and attempts to describe dark energy in terms of the vacuum energy of quantum field theory lead to a discrepancy of around $120$ orders of magnitude.\footnote{This is known as the cosmological constant problem and has been described as the ``worst theoretical prediction in the history of physics''~\cite{hobson2006general}.}

If the Big Bang had produced equal amounts of matter and antimatter, the two would annihilate in a burst of light, and planets, stars and galaxies would simply not exist. The clear evidence to the contrary indicates the universe's favouritism towards matter. One of the necessary ingredients for matter to win out over antimatter is the violation of charge-parity (\CP) symmetry~\cite{Sakharov:1967dj}. Although the SM incorporates \CP violation, the excess of matter it generates falls many orders of magnitude short of the observed asymmetry~\cite{Gavela:1993ts}.

%Besides these glaring issues, the SM has more subtle flaws. Naturalness, 19 free parameters

Due to these shortcomings, the SM is widely regarded as a low-energy approximation of a more fundamental theory. However, as the SM is so frustratingly successful, the form this theory takes is unknown. A feature that is required in order to explain the matter-antimatter asymmetry is new sources of \CP violation. As \CP violation is small in the SM, new physics contributions may be able to stand on an equal footing. The study of \CP violation therefore represents one of the most promising routes to the discovery of new physics. 

One of the primary goals of the \lhcb experiment at the \lhc is to make precise measurements of \CP-violating observables in the decays of particles containing heavy quarks. The decays of charged $B$ mesons,\footnote{Mesons are bound states of quark-antiquark pairs. Three-quark combinations are known as baryons.} \Bp or \Bc, to two charm mesons provide a particularly rich laboratory for the study of \CP violation due to the potential for large effects. Moreover, the origin of these effects differs between the two $B$ meson species, with the \Bc decays expected to have large SM contributions~\cite{MASETTI1992160,PhysRevD.62.057503,Giri:2001be,Giri:2006cw,Ivanov:2002un,Kiselev:2003ds}, while the \Bp decays are sensitive to new physics~\cite{Kim:2008ex,Xu2016}. The study of these decays therefore provides a two-pronged probe into the nature of \CP violation.

While the \Bp decays to two charm mesons are experimentally well-established, the \Bc decays have never before been observed. The study of \Bc decays is challenging for several reasons. First, the \Bc meson is composed of two heavy quarks, so its production is much rarer than that of the \Bp meson. Second, its lifetime is short, resulting in a lower reconstruction efficiency. Third, the branching fractions of the relevant decays are predicted to be two orders of magnitude smaller for \Bc than for \Bp~\cite{PhysRevD.86.074019,Kiselev:2003ds,Ivanov:2002un,PhysRevD.73.054024}. The properties of the different $B$ mesons are summarised in table~\ref{tab:Bmesons}.

%The study of the two $B$ species is complementary since the \Bc decays are expected to have large \CP violation effects from the SM~\cite{MASETTI1992160,PhysRevD.62.057503,Giri:2001be,Giri:2006cw,Ivanov:2002un,Kiselev:2003ds}, while the \Bp decays are sensitive to new physics~\cite{Kim:2008ex,Xu2016}. However, observation of \Bc decays is experimentally challenging since the simultaneous production of two heavy quark pairs is required and the short lifetime reduces the reconstruction efficiency. The properties of the different $B$ mesons are listed in table~\ref{tab:Bmesons}.

\begin{table}[tb]
\centering
  \begin{tabular}{lccc}
  \toprule
   Meson & Quark content & Mass$~(\mevcc)$ & Lifetime$~(\ps)$\\
  \midrule
  \Bp & $\bar{b} u$ & $5279.33\pm 0.13$ & $1.638\pm 0.004$\\
  \Bz & $\bar{b} d$ & $5279.64\pm 0.13$ & $1.519\pm 0.004$\\
  \Bc & $\bar{b} c$ & $6274.9\pm 0.8$ & $0.510\pm 0.009$\\
  \Bs & $\bar{b} s$ & $5366.88\pm 0.17$ & $1.527\pm 0.011$\\
  \bottomrule
  \end{tabular}
    \caption{The quark content, mass and lifetime of the different $B$ mesons~\cite{PDG2018}.}
	\label{tab:Bmesons}
\end{table}
\vspace{-0.4cm}

%The \lhc was built to search for clues among the debris of the highest energy particle collisions ever performed. ``Direct'' searches, primarily conducted by the \atlas and \cms experiments, aim to detect the production of entirely new particles, however, this is limited by the collision energy. ``Indirect'' searches, such as those by the \lhcb experiment, instead search for the signatures of heavy virtual particles on SM processes and hence, don't suffer from the same constraints.
