\select@language {english}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Theoretical motivation}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Baryon asymmetry}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Antimatter}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Baryogenesis}{7}{subsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.2.1}Sakharov conditions}{8}{subsubsection.2.1.2.1}
\contentsline {subsubsection}{\numberline {2.1.2.2}Baryogenesis within the Standard Model}{9}{subsubsection.2.1.2.2}
\contentsline {paragraph}{\nonumberline Baryon number violation}{9}{section*.4}
\contentsline {paragraph}{\nonumberline $C$ and {\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace violation}{9}{section*.5}
\contentsline {paragraph}{\nonumberline Interactions out of thermal equilibrium}{10}{section*.6}
\contentsline {subsubsection}{\numberline {2.1.2.3}Baryogenesis beyond the Standard Model}{11}{subsubsection.2.1.2.3}
\contentsline {paragraph}{\nonumberline Leptogenesis}{11}{section*.8}
\contentsline {paragraph}{\nonumberline Supersymmetry}{12}{section*.9}
\contentsline {paragraph}{\nonumberline Grand Unified Theories}{12}{section*.10}
\contentsline {subsubsection}{\numberline {2.1.2.4}Experimental prospects}{13}{subsubsection.2.1.2.4}
\contentsline {section}{\numberline {2.2}Charge-parity violation}{13}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Types of {\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace violation}{13}{subsection.2.2.1}
\contentsline {paragraph}{\nonumberline {\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace violation in the decay}{14}{section*.11}
\contentsline {paragraph}{\nonumberline {\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace violation in mixing}{15}{section*.12}
\contentsline {paragraph}{\nonumberline {\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace violation in the interference of mixing and decay}{15}{section*.13}
\contentsline {subsection}{\numberline {2.2.2}The CKM paradigm}{16}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.1}Representations}{17}{subsubsection.2.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2.2}The unitarity triangle}{18}{subsubsection.2.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.3}Current experimental constraints}{19}{subsubsection.2.2.2.3}
\contentsline {section}{\numberline {2.3}Measuring \mymath {\@Oldgamma }\xspace }{22}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}${\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace \to DK^+$}{22}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}GLW method}{23}{subsubsection.2.3.1.1}
\contentsline {subsubsection}{\numberline {2.3.1.2}ADS method}{25}{subsubsection.2.3.1.2}
\contentsline {subsubsection}{\numberline {2.3.1.3}GGSZ method}{26}{subsubsection.2.3.1.3}
\contentsline {subsection}{\numberline {2.3.2}${\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace \to D_{(s)}^{(*)+}D^{(*)}$}{27}{subsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.2.1}GLW method}{29}{subsubsection.2.3.2.1}
\contentsline {subsubsection}{\numberline {2.3.2.2}Experimental prospects}{30}{subsubsection.2.3.2.2}
\contentsline {section}{\numberline {2.4}The {\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace asymmetry in {\ensuremath {\unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^+_{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}}}\xspace {\ensuremath {{\ensuremath {\offsetoverline {\ensuremath {D}\xspace }}}\xspace {}^0}}\xspace }}\xspace }}\xspace decays}{34}{section.2.4}
\contentsline {chapter}{\numberline {3}The \unhbox \voidb@x \hbox {LHCb}\xspace experiment}{37}{chapter.3}
\contentsline {section}{\numberline {3.1}\unhbox \voidb@x \hbox {CERN}\xspace }{37}{section.3.1}
\contentsline {section}{\numberline {3.2}The \unhbox \voidb@x \hbox {LHC}\xspace }{38}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}The accelerator complex}{40}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Running conditions}{42}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}The \unhbox \voidb@x \hbox {LHCb}\xspace detector}{44}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Tracking}{46}{subsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.1.1}Vertex locator}{46}{subsubsection.3.3.1.1}
\contentsline {subsubsection}{\numberline {3.3.1.2}Magnet}{50}{subsubsection.3.3.1.2}
\contentsline {subsubsection}{\numberline {3.3.1.3}Tracking stations}{50}{subsubsection.3.3.1.3}
\contentsline {paragraph}{\nonumberline Tracker Turicensis}{51}{section*.37}
\contentsline {paragraph}{\nonumberline Inner tracker}{52}{section*.39}
\contentsline {paragraph}{\nonumberline Outer tracker}{52}{section*.41}
\contentsline {subsubsection}{\numberline {3.3.1.4}Track reconstruction and performance}{53}{subsubsection.3.3.1.4}
\contentsline {subsection}{\numberline {3.3.2}Particle identification}{55}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Ring imaging Cherenkov detectors}{55}{subsubsection.3.3.2.1}
\contentsline {paragraph}{\nonumberline RICH1\xspace }{56}{section*.46}
\contentsline {paragraph}{\nonumberline RICH2\xspace }{57}{section*.48}
\contentsline {paragraph}{\nonumberline Performance}{58}{section*.49}
\contentsline {subsubsection}{\numberline {3.3.2.2}Calorimeters}{59}{subsubsection.3.3.2.2}
\contentsline {subsubsection}{\numberline {3.3.2.3}Muon system}{61}{subsubsection.3.3.2.3}
\contentsline {subsection}{\numberline {3.3.3}Trigger}{62}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}Level 0}{62}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}High level trigger}{63}{subsubsection.3.3.3.2}
\contentsline {subsection}{\numberline {3.3.4}Software}{65}{subsection.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.4.1}Simulation}{66}{subsubsection.3.3.4.1}
\contentsline {subsubsection}{\numberline {3.3.4.2}Digitisation}{66}{subsubsection.3.3.4.2}
\contentsline {subsubsection}{\numberline {3.3.4.3}Trigger}{67}{subsubsection.3.3.4.3}
\contentsline {subsubsection}{\numberline {3.3.4.4}Reconstruction}{67}{subsubsection.3.3.4.4}
\contentsline {subsubsection}{\numberline {3.3.4.5}Analysis}{67}{subsubsection.3.3.4.5}
\contentsline {chapter}{\numberline {4}Event selection}{69}{chapter.4}
\contentsline {section}{\numberline {4.1}Data and simulation samples}{70}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Data}{70}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Simulation}{70}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Common selection variables}{71}{section.4.2}
\contentsline {section}{\numberline {4.3}Trigger}{73}{section.4.3}
\contentsline {paragraph}{\nonumberline L0}{74}{section*.56}
\contentsline {paragraph}{\nonumberline HLT1\xspace }{74}{section*.58}
\contentsline {paragraph}{\nonumberline HLT2\xspace }{75}{section*.59}
\contentsline {section}{\numberline {4.4}Stripping}{75}{section.4.4}
\contentsline {section}{\numberline {4.5}Calibrations, corrections, constraints and comparisons}{78}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Calibrations to data}{79}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Corrections to simulation}{79}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Constrained $B$ mass}{80}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Data/simulation comparison}{80}{subsection.4.5.4}
\contentsline {section}{\numberline {4.6}Preselection}{81}{section.4.6}
\contentsline {section}{\numberline {4.7}Multivariate analysis}{84}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Investigation of background properties}{86}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Training}{88}{subsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.2.1}Signal and background samples}{88}{subsubsection.4.7.2.1}
\contentsline {subsubsection}{\numberline {4.7.2.2}Input variables}{89}{subsubsection.4.7.2.2}
\contentsline {subsubsection}{\numberline {4.7.2.3}Methods}{90}{subsubsection.4.7.2.3}
\contentsline {paragraph}{\nonumberline Boosted decision tree}{90}{section*.70}
\contentsline {paragraph}{\nonumberline Multilayer perceptron}{92}{section*.72}
\contentsline {paragraph}{\nonumberline Performance}{93}{section*.74}
\contentsline {subsubsection}{\numberline {4.7.2.4}Overtraining}{93}{subsubsection.4.7.2.4}
\contentsline {subsection}{\numberline {4.7.3}Optimisation}{94}{subsection.4.7.3}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace decays}{95}{section*.77}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace decays}{97}{section*.78}
\contentsline {chapter}{\numberline {5}Search for {\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace decays to two charm mesons}{99}{chapter.5}
\contentsline {section}{\numberline {5.1}Analysis strategy}{99}{section.5.1}
\contentsline {section}{\numberline {5.2}Fit model}{100}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}\unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{({\ensuremath {\ensuremath {c}\xspace }}\xspace )}^+}}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^+_{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}}}\xspace {\ensuremath {\ensuremath {D}\xspace }}\xspace }}\xspace model}{101}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}\unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^{*+}_{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}}}\xspace {\ensuremath {\ensuremath {D}\xspace }}\xspace ,{\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^+_{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}}}\xspace {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^*}}\xspace }}\xspace model}{102}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}\unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^{*+}_{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}}}\xspace {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^*}}\xspace }}\xspace model}{104}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}\unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {K}\xspace }}\xspace ^+}}\xspace {\ensuremath {{\ensuremath {\ensuremath {K}\xspace }}\xspace ^-}}\xspace {\ensuremath {{\ensuremath {\ensuremath {\mymath {\@Oldpi }\xspace }\xspace }}\xspace ^+}}\xspace {\ensuremath {{\ensuremath {\offsetoverline {\ensuremath {D}\xspace }}}\xspace {}^0}}\xspace }}\xspace model}{106}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}Efficiencies}{109}{section.5.3}
\contentsline {section}{\numberline {5.4}Mass fits}{110}{section.5.4}
\contentsline {section}{\numberline {5.5}Fit validation}{113}{section.5.5}
\contentsline {section}{\numberline {5.6}Systematic uncertainties}{118}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Systematic uncertainties on the {\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace yields}{118}{subsection.5.6.1}
\contentsline {paragraph}{\nonumberline Signal shape}{118}{section*.93}
\contentsline {paragraph}{\nonumberline Signal model}{118}{section*.94}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace mass}{119}{section*.95}
\contentsline {paragraph}{\nonumberline Background model}{120}{section*.96}
\contentsline {paragraph}{\nonumberline Fit bias}{120}{section*.97}
\contentsline {paragraph}{\nonumberline Signal composition}{120}{section*.98}
\contentsline {paragraph}{\nonumberline Polarisation}{120}{section*.99}
\contentsline {subsection}{\numberline {5.6.2}Systematic uncertainties on the normalisation}{121}{subsection.5.6.2}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace statistics}{121}{section*.101}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace signal shape}{122}{section*.102}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace signal model}{122}{section*.103}
\contentsline {paragraph}{\nonumberline Background model}{122}{section*.104}
\contentsline {paragraph}{\nonumberline \unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {K}\xspace }}\xspace ^+}}\xspace {\ensuremath {{\ensuremath {\ensuremath {K}\xspace }}\xspace ^-}}\xspace {\ensuremath {{\ensuremath {\ensuremath {\mymath {\@Oldpi }\xspace }\xspace }}\xspace ^+}}\xspace {\ensuremath {{\ensuremath {\offsetoverline {\ensuremath {D}\xspace }}}\xspace {}^0}}\xspace }}\xspace }{122}{section*.105}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace lifetime}{123}{section*.106}
\contentsline {paragraph}{\nonumberline Particle identification}{123}{section*.107}
\contentsline {paragraph}{\nonumberline {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^0}}\xspace model}{123}{section*.108}
\contentsline {paragraph}{\nonumberline Simulation statistics}{124}{section*.109}
\contentsline {paragraph}{\nonumberline Signal composition}{124}{section*.110}
\contentsline {paragraph}{\nonumberline Polarisation}{124}{section*.111}
\contentsline {paragraph}{\nonumberline ${\ensuremath {\mathcal {B}}}\xspace (\unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^{*+}}}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^+}}\xspace {\ensuremath {{\ensuremath {\ensuremath {\mymath {\@Oldpi }\xspace }\xspace }}\xspace ^0}}\xspace /\mymath {\@Oldgamma }\xspace }}\xspace )$}{124}{section*.112}
\contentsline {section}{\numberline {5.7}Branching fractions}{125}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Limits}{126}{subsection.5.7.1}
\contentsline {paragraph}{\nonumberline Asymptotic CL$_s$ method}{127}{section*.113}
\contentsline {paragraph}{\nonumberline Results}{128}{section*.114}
\contentsline {subsection}{\numberline {5.7.2}Interpretation}{130}{subsection.5.7.2}
\contentsline {chapter}{\numberline {6}Measurement of the {\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace asymmetry in {\ensuremath {\unhbox \voidb@x \hbox {\ensuremath {{\ensuremath {{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace ^+}}\xspace }}\xspace \tmspace -\thinmuskip {.1667em}\to {\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^+_{({\ensuremath {\ensuremath {s}\xspace }}\xspace )}}}\xspace {\ensuremath {{\ensuremath {\offsetoverline {\ensuremath {D}\xspace }}}\xspace {}^0}}\xspace }}\xspace }}\xspace decays}{133}{chapter.6}
\contentsline {section}{\numberline {6.1}Analysis strategy}{133}{section.6.1}
\contentsline {section}{\numberline {6.2}Raw asymmetry}{134}{section.6.2}
\contentsline {section}{\numberline {6.3}Detection asymmetry}{137}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Trigger asymmetry}{137}{subsection.6.3.1}
\contentsline {paragraph}{\nonumberline L0 global TIS}{139}{section*.120}
\contentsline {paragraph}{\nonumberline L0 hadron TOS}{139}{section*.121}
\contentsline {subsection}{\numberline {6.3.2}Tracking asymmetry}{141}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}PID asymmetry}{144}{subsection.6.3.3}
\contentsline {subsection}{\numberline {6.3.4}Kaon detection asymmetry}{145}{subsection.6.3.4}
\contentsline {section}{\numberline {6.4}Production asymmetry}{147}{section.6.4}
\contentsline {section}{\numberline {6.5}Results}{148}{section.6.5}
\contentsline {chapter}{\numberline {7}Summary and outlook}{151}{chapter.7}
\contentsline {section}{\numberline {7.1}Summary}{151}{section.7.1}
\contentsline {section}{\numberline {7.2}Outlook}{153}{section.7.2}
\contentsline {chapter}{\numberline {A}Data/simulation comparison}{155}{appendix.A}
\contentsline {chapter}{\numberline {B}MVA input variables}{163}{appendix.B}
\contentsline {chapter}{\numberline {C}Fit results}{165}{appendix.C}
\contentsline {section}{\numberline {C.1}{\ensuremath {{\ensuremath {\ensuremath {B}\xspace }}\xspace _{\ensuremath {\ensuremath {c}\xspace }}\xspace ^+}}\xspace search}{165}{section.C.1}
\contentsline {section}{\numberline {C.2}{\ensuremath {C\tmspace -\thinmuskip {.1667em}P}}\xspace asymmetry measurement}{166}{section.C.2}
\contentsline {chapter}{Bibliography}{167}{appendix*.141}
\contentsline {chapter}{List of figures}{183}{appendix*.142}
\contentsline {chapter}{List of tables}{191}{appendix*.143}
