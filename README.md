# README

"Doubly charmed _B_ decays with the LHCb experiment" PhD thesis.

Available on [CDS](https://cds.cern.ch/record/2683122?ln=en), [Apollo](https://www.repository.cam.ac.uk/handle/1810/297741) and [inspirehep](http://inspirehep.net/record/1754076?ln=en).

# Citation
Tully, A. M. (2019). Doubly charmed B decays with the LHCb experiment (Doctoral thesis). https://doi.org/10.17863/CAM.44796
