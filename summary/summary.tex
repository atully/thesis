\chapter{Summary and outlook}
\label{chap:summary}

\chapterquote{Endings to be useful must be inconclusive.}%
{Samuel R. Delany}%: Source

The SM provides an incredibly successful description of nature, however, it is known to be incomplete. One of its more glaring flaws is its prediction that we should not exist, or rather, that almost all matter and antimatter should have annihilated in the early universe. This is because the amount of \CP violation included in the SM is too small to generate a large enough baryon asymmetry to explain astronomical observations~\cite{Gavela:1993ts}. Therefore, there must be additional, new physics sources of \CP violation and physicists at the \lhcb experiment are studying the decays of particles containing heavy quarks to try and find them. In this thesis, two such analyses involving doubly charmed $B$ decays have been described. In this chapter, the results are summarised (section~\ref{sec:summary}), followed by a discussion of the future prospects (section~\ref{sec:outlook}).
%It predicted the existence of several particles prior to their discovery and has been verified to high precision across a wide range of experimental measurements. 

\section{Summary}
\label{sec:summary}

\CP violation is included in the SM as a complex phase in the $3\times 3$ unitary CKM matrix. The unitarity of the CKM matrix provides several constraints that can be represented geometrically as triangles. The least well-known angle of the unitarity triangle with sides of similar sizes is \gamma. It is also the only angle that can be measured with tree-level decays, making it an important SM benchmark since it is unlikely to be affected by new physics. Measurements of \gamma have been made with \Bp, \Bz and \Bs decays, but not yet with \Bc decays~\cite{LHCb-CONF-2018-002}. Sensitivity to \gamma arises in the interference of $b\to c$ and $b \to u$ decays, which correspond to decays of \decay{\Bc}{\DporDsorstar\DzborDzbstar} and \decay{\Bc}{\DporDsorstar\DzorDzstar}, respectively~\cite{MASETTI1992160,PhysRevD.62.057503,Giri:2001be,Giri:2006cw,Ivanov:2002un,Kiselev:2003ds}.

A search for \decay{\Bc}{\DporDsorstar\DorDstar} decays has been performed using Run I data collected by the \lhcb experiment~\cite{LHCb-PAPER-2017-045}. The \Bc decays were measured relative to the high-yield \BpDporDsDzb normalisation channel. No evidence for signal was found. The measured branching fractions and upper limits set at $90\,(95\%)$ for the fully reconstructed \Bc decays are
\begin{align} 
\fcfu\frac{\BF(\decay{\Bc}{\Ds\Dzb})}{\BF(\decay{\Bp}{\Ds\Dzb})} &= (\phantom{-}3.0\pm3.7)\times 10^{-4}\;[< 0.9\,(1.1)\times 10^{-3}],\\
\fcfu\frac{\BF(\decay{\Bc}{\Ds\Dz})}{\BF(\decay{\Bp}{\Ds\Dzb})} &= ( -3.8\pm2.6)\times 10^{-4}\;[< 3.7\,(4.7)\times 10^{-4}],\\
\fcfu\frac{\BF(\decay{\Bc}{\Dp\Dzb})}{\BF(\decay{\Bp}{\Dp\Dzb})} &= (\phantom{-}8.0\pm7.5)\times 10^{-3}\;[< 1.9\,(2.2)\times 10^{-2}],\\
\fcfu\frac{\BF(\decay{\Bc}{\Dp\Dz})}{\BF(\decay{\Bp}{\Dp\Dzb})} &= (\phantom{-}2.9\pm5.3)\times 10^{-3}\;[< 1.2\,(1.4)\times 10^{-2}].
\end{align}
The results for the \Bc decays with one excited charm meson in the final state are
\vspace{-0.9cm}
\begin{adjustwidth}{0cm}{-1cm}
\begin{align} 
\hspace{-2.55cm}\fcfu\frac{\BF(\decay{\Bc}{\Dss\Dzb})+\BF(\decay{\Bc}{\Ds\Dstarzb})}{\BF(\decay{\Bp}{\Ds\Dzb})} &= ( -0.1\pm1.5)\times 10^{-3}\;[< 2.8\,(3.4)\times 10^{-3}],\\
\hspace{-2.55cm}\fcfu\frac{\BF(\decay{\Bc}{\Dss\Dz})+\BF(\decay{\Bc}{\Ds\Dstarz})}{\BF(\decay{\Bp}{\Ds\Dzb})} &= ( -0.3\pm1.9)\times 10^{-3}\;[< 3.0\,(3.6)\times 10^{-3}],\\
\hspace{-2.55cm}\fcfu\frac{\BF(\decay{\Bc}{(\Dstarp\to\Dp\piz/\gamma)\Dzb})+\BF(\decay{\Bc}{\Dp\Dstarzb})}{\BF(\decay{\Bp}{\Dp\Dzb})}&= (\phantom{-}0.2\pm3.2)\times 10^{-2}\;[< 5.5\,(6.6)\times 10^{-2}],\label{eq:BcresultsDstarpDzb}\\
\hspace{-2.55cm}\fcfu\frac{\BF(\decay{\Bc}{(\Dstarp\to\Dp\piz/\gamma)\Dz})+\BF(\decay{\Bc}{\Dp\Dstarz})}{\BF(\decay{\Bp}{\Dp\Dzb})}  &= (-1.5\pm1.7)\times 10^{-2}\;[< 2.2\,(2.8)\times 10^{-2}].\label{eq:BcresultsDstarpDz}
\end{align}
\end{adjustwidth}
The results for the \Bc decays with two excited charm mesons in the final state are
\begin{align} 
\fcfu\frac{\BF(\decay{\Bc}{\Dss\Dstarzb})}{\BF(\decay{\Bp}{\Ds\Dzb})} &= (\phantom{-}3.2\pm4.3)\times 10^{-3}\;[< 1.1\,(1.3)\times 10^{-2}],\\
\fcfu\frac{\BF(\decay{\Bc}{\Dss\Dstarz})}{\BF(\decay{\Bp}{\Ds\Dzb})} &= (\phantom{-}7.0\pm9.2)\times 10^{-3}\;[< 2.0\,(2.4)\times 10^{-2}],\\
\fcfu\frac{\BF(\decay{\Bc}{\Dstarp\Dstarzb})}{\BF(\decay{\Bp}{\Dp\Dzb})} &= (\phantom{-}3.4\pm2.3)\times 10^{-1}\;[< 6.5\,(7.3)\times 10^{-1}],\\
\fcfu\frac{\BF(\decay{\Bc}{\Dstarp\Dstarz})}{\BF(\decay{\Bp}{\Dp\Dzb})} &= (-4.1\pm9.1)\times 10^{-2}\;[< 1.3\,(1.6)\times 10^{-1}].
\end{align}
The limits are an order of magnitude greater than the largest theoretical prediction. Hence, the experimental measurements and theoretical predictions are consistent.

A measurement of the \CP asymmetry in \BpDporDsDzb decays has been performed using Run I data collected by the \lhcb experiment~\cite{LHCb-PAPER-2018-007}. A non-zero \CP asymmetry requires two interfering contributions with different weak and strong phases. Decays of \BpDporDsDzb receive contributions from both tree-level and loop-level diagrams and are predicted to have a \CP asymmetry of $\mathcal{O}(1\%)$ in the SM~\cite{Li:2009xf,Fu:2011zzo,Lu:2010gg,Kim:2008ex,Xu2016}. New physics can enhance the \CP asymmetry in these decays to $\mathcal{O}(10\%)$~\cite{Kim:2008ex,Xu2016}. The \CP asymmetry was measured by determining the asymmetry in the signal yields, then correcting this to account for the production and detection asymmetries. The results are
\begin{align}
	\mathcal{A}^{\CP}(\BpDsDzb) &= (-0.4\pm 0.5\pm 0.5)\%,\\
	\mathcal{A}^{\CP}(\BpDpDzb) &= (+2.3\pm 2.7\pm 0.4)\%,
\end{align}
where the first uncertainty is statistical and the second systematic. This is the first measurement of $\mathcal{A}^{\CP}(\BpDsDzb)$ and the measurement of $\mathcal{A}^{\CP}(\BpDpDzb)$ improves upon the precision of the world average value by a factor of 2.6~\cite{PDG2018}. The results are consistent with \CP symmetry and can be used to constrain new physics contributions~\cite{Kim:2008ex,Xu2016}. All in all, the SM wins again.

\section{Outlook}
\label{sec:outlook}

To date, the \lhcb detector has only collected $\sim 1\%$ of its total expected yield in the channels of interest (see table~\ref{tab:Bpyields}). The immediate next step is to repeat the analyses with the addition of Run II data, which is expected to have yields 3.5 times the size of those in the Run I data. With the combined Run I and II data set, a yield of $6.5$ events is predicted in the channel with the largest theory prediction, \BcDpDzb, using optimistic assumptions (see table~\ref{tab:Bcyieldestimates}). With an upward fluctuation, evidence of this decay could be seen. With the addition of Run III data in 2023, this estimation increases to 47 events, at which point observation is likely. Once the branching fraction of \BcDpDzb decays has been measured, more accurate predictions can be made for \BcDsD decays. In 2033, when the full data set up to Run V has been collected, it may be possible to make a measurement of $\gamma$ with an uncertainty of around $20^\circ$.

Currently, some sensitivity is lost in the \decay{\Bc}{\Dstarp\DorDstar} channels since the decay of \DpstDzPp with a branching fraction of $\sim 68\%$ is not being reconstructed. However, this mode could be added in a future update. The \decay{\Bc}{\Dstarp\D} decay would therefore be fully reconstructed and have a sensitivity similar to \BcDporDsD decays. The analysis of this mode would also provide information to help distinguish \decay{\Bc}{\Dstarp\D} and \decay{\Bc}{\Dp\Dstar} in the limits set with partially reconstructed \decay{\Dstarp}{\Dp \piz/\gamma} decays (see equations~\ref{eq:BcresultsDstarpDzb} and~\ref{eq:BcresultsDstarpDz}). In addition, the \CP asymmetry in \decay{\Bp}{\Dstarp\Dzb} decays could be measured.

The dominant systematic uncertainty in the partially reconstructed \Bc modes is the choice of background model due to their wide $B$ mass distributions (see table~\ref{tab:yieldsyst}). Instead of placing a requirement on the MVA response based on a figure of merit, the fit could be performed simultaneously over bins of MVA response. The addition of low signal purity bins to the fit would help to constrain the background model, reducing the systematic uncertainty.

It is an exciting time to be a particle physicist. The field has made enormous leaps forward, both theoretically and experimentally, since Ernest Rutherford discovered the structure of the atom just over one hundred years ago. The \lhc, the most powerful particle accelerator ever built, is subjecting the SM to its most rigorous testing yet. The \lhcb experiment now faces some healthy competition from the \belle II experiment~\cite{Abe:2010gxa}, a ``$B$ factory'' that started collecting data earlier this year. Although \belle II doesn't operate at a high enough centre-of-mass energy to produce \Bc mesons, it will be able to make measurements of many key flavour physics observables using \Bp, \Bz and \Bs decays~\cite{Kou:2018nap,Albrecht:2017odf}. If there is new physics at the electroweak scale, it hopefully won't be able to hide much longer.

