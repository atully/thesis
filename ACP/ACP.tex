\chapter{Measurement of the \CP asymmetry in \BpDporDsDzb decays}
\label{chap:ACP}

\graphicspath{{ACP/figs/}}

\chapterquote{Symmetry is overrated. Overrated is symmetry.}%
{Larry Wall}%: Source

This chapter describes the measurement of the \CP asymmetry in \BpDporDsDzb decays using Run I data collected by the \lhcb experiment and has been published in reference~\cite{LHCb-PAPER-2018-007}. Some predictions for $\mathcal{A}^{\CP}(\BpDporDsDzb)$ in the SM are listed in table~\ref{tab:ACPpredictions}. Large values of $\mathcal{O}(10\%)$ are predicted by some new physics models~\cite{Kim:2008ex,Xu2016}. First, an overview of the analysis strategy is given (section~\ref{sec:Bpanastrat}). This is followed by a description of the raw asymmetry (section~\ref{sec:rawasym}), detection asymmetry (section~\ref{sec:detectionasym}), and production asymmetry (section~\ref{sec:productionasym}) and lastly, the results for the \CP asymmetry (section~\ref{sec:results}) are presented.

\section{Analysis strategy}
\label{sec:Bpanastrat}

The \CP asymmetry in \BpDporDsDzb decays is defined as
\begin{equation}
	\mathcal{A}^{\CP}(\BpDporDsDzb) \equiv \frac{\Gamma(\Bm\to D^-_{(s)}\Dz)-\Gamma(\Bp\to D^+_{(s)}\Dzb)}{\Gamma(\Bm\to D^-_{(s)}\Dz)+\Gamma(\Bp\to D^+_{(s)}\Dzb)}.
\end{equation}
where $\Gamma$ is the decay rate. The determination of $\mathcal{A}^{\CP}(\BpDporDsDzb)$ is based on the raw asymmetry,
\begin{equation}
	\label{eq:rawasym}
	A_{\rm raw} \equiv \frac{N(\Bm\to D^-_{(s)}\Dz)-N(\Bp\to D^+_{(s)}\Dzb)}{N(\Bm\to D^-_{(s)}\Dz)+N(\Bp\to D^+_{(s)}\Dzb)},
\end{equation}
where $N$ stand for the signal yields. The raw asymmetry contains contributions from \CP violation, as well as experimental effects such as the collision environment and the interaction of the final state particles with the detector. If the asymmetries are small such that terms containing products of asymmetries can be neglected, the \CP asymmetry can be expressed as
\begin{equation}
	\label{eq:asymrel}
	\mathcal{A}^{\CP} = A_{\rm raw} - A_{D} - A_{P},
\end{equation}
where the detection asymmetry, $A_D$, is the asymmetry in the efficiencies, \varepsilon,
\begin{equation}
	\label{eq:detasym}
	A_{D} \equiv \frac{\varepsilon(\Bm\to D^-_{(s)}\Dz)-\varepsilon(\Bp\to D^+_{(s)}\Dzb)}{\varepsilon(\Bm\to D^-_{(s)}\Dz)+\varepsilon(\Bp\to D^+_{(s)}\Dzb)},
\end{equation}
and the production asymmetry, $A_P$, is the asymmetry of the \Bp and \Bm production cross-sections, \sigma,
\begin{equation}
	\label{eq:prodasym}
	A_{P} \equiv \frac{\sigma(\Bm)-\sigma(\Bp)}{\sigma(\Bm)+\sigma(\Bp)}.
\end{equation}

\section{Raw asymmetry}
\label{sec:rawasym}

To determine the yields, fits are performed to the constrained mass distributions of \BpDporDsDzb candidates, separated by charge, in the range $5230<m(\DporDs\Dzb)<5330\mevcc$. The fit model contains three components: the \BpDporDsDzb signal; the single charm \decay{\Bp}{\Km\pip\pip\Dzb} background; and the combinatorial background. The parametrisation of the \BpDporDsDzb and \decay{\Bp}{\Km\pip\pip\Dzb} models is the same as in the \Bc search (see sections~\ref{sec:Bc2DD_shapes} and \ref{sec:charmlessmodel}), however, small differences are expected since the shape parameters are derived from samples with differing MVA requirements. For the \decay{\Bp}{\Km\pip\pip\Dzb} model, the yield is determined separately for each charge, whereas the width is assumed to be charge independent. For the combinatorial background, an exponential function is used. This is a simpler model than the one used in the \Bc search (the sum of an exponential function and a constant) due to the reduced fit range.

Before unblinding, the \BpDpDzb mode was separated by charge randomly based on the run number. Only after the analysis procedure was finalised and internally reviewed was the fit performed on the unblinded data. The results of the unblinded fits are shown in figure~\ref{fig:ACPyields} with the corresponding yields and raw asymmetries listed in table~\ref{tab:Araw}. The fitted values of all parameters allowed to float can be seen in appendix~\ref{tab:ACPfitresults}. Separate fits are performed for each of the \Dzb channels and the results are combined by summing the yields and adding the uncertainties in quadrature. The yields are larger for the \BpDsDzb mode than for the \BpDpDzb mode due to the factor $\sim 25$ larger branching fraction~\cite{PDG2018}, resulting in smaller statistical uncertainties. The raw asymmetry in \BpDsDzb decays is significantly non-zero, whereas the raw asymmetry in \BpDpDzb is compatible with zero.

Although the choice of fit model may result in small biases in the yields, this is expected to affect both charges in the same way and hence cancel to first order in the measurement of the raw asymmetry. To check for any residual bias, the fit is repeated with several variations: the sum of two Gaussian functions as the \Bp signal model; a single Gaussian function as the \Bp signal model; a linear function as the combinatorial background model; and a uniform function as the combinatorial background model. For the high-statistics \BpDsDzb mode, the effect on the raw asymmetry is below $0.1\%$. Slightly larger variations of $0.1-0.2\%$ are observed in the \BpDpDzb mode, however, this is expected due to the larger statistical uncertainties. Overall, no bias due to the fit model is observed.

\begin{figure}[p]
\includegraphics[width=0.48\textwidth]{Bm2DsDz_Kpi}
\includegraphics[width=0.48\textwidth]{Bp2DsDz_Kpi}\\
\includegraphics[width=0.48\textwidth]{Bm2DsDz_Kpipipi}
\includegraphics[width=0.48\textwidth]{Bp2DsDz_Kpipipi}\\
\includegraphics[width=0.48\textwidth]{Bm2DpDz_Kpi}
\includegraphics[width=0.48\textwidth]{Bp2DpDz_Kpi}\\
\includegraphics[width=0.48\textwidth]{Bm2DpDz_Kpipipi}
\includegraphics[width=0.48\textwidth]{Bp2DpDz_Kpipipi}
\caption{Constrained mass distributions of \BpDporDsDzb candidates, separated by charge.
The top and third row plots are with \DKpi decays and the second and last row are with \DKpipipi decays.}
%The overlaid curves show the fits described in the text.
%The fits include a component for the combinatorial background, which are not explicitly shown in the plots.
\label{fig:ACPyields}
\end{figure}


\begin{table}[tbh]
  \caption{Yields and raw asymmetries for \BpDsDzb decays.}
  \begin{center}
  \begin{tabular}{lccc}
  \toprule
  Channel                                             & $N(\Bm)$    & $N(\Bp)$ & $A_{\rm raw} (\%)$ \\
  \midrule
  \BpDsDzb, \DKpi     & $ 13659\pm 129$ & $14209\pm 132$ & $ -2.0\pm0.7$ \\
  \BpDsDzb, \DKpipipi & $\phantom{0}7717\pm 103$ & $\phantom{0}7945\pm 104$ & $ -1.5\pm0.9$ \\
  \BpDsDzb, combined  & $ 21375\pm 165$ & $ 22153\pm 168$ & $ -1.8\pm0.5$ \\
  \midrule
  \BpDpDzb, \DKpi     & $\phantom{00}678\pm\phantom{0}32$ & $\phantom{00}660\pm\phantom{0}31$ & $\phantom{-}1.3\pm3.3$ \\
  \BpDpDzb, \DKpipipi & $\phantom{00}369\pm\phantom{0}24$ & $\phantom{00}345\pm\phantom{0}24$ & $\phantom{-}3.4\pm4.7$ \\
  \BpDpDzb, combined  & $\phantom{0}1047\pm\phantom{0}40$ & $\phantom{0}1005\pm\phantom{0}39$ & $\phantom{-}2.0\pm2.7$ \\
  \bottomrule
  \end{tabular}
  \end{center}
\label{tab:Araw}
\end{table}
\vspace{-0.4cm}

\section{Detection asymmetry}
\label{sec:detectionasym}

The presence of a left-right asymmetry in the detection efficiencies can be determined by measuring the raw asymmetry separately for each magnet polarity. If one side of the detector is more efficient than the other, this favours particles of a particular charge that are bent into that side by the magnetic field, creating an asymmetry. This effect is cancelled when combining magnet polarities. Time-dependent detection asymmetries can be identified by determining the raw asymmetry for different data-taking periods. The raw asymmetry for the high-statistics \BpDsDzb mode, separated by magnet polarity and data-taking year, is shown in table~\ref{tab:Arawsplit}. None of the asymmetries show a significant difference from the combination, indicating that the left-right and time-dependent asymmetries are small.

\begin{table}[p]
  \caption{Yields and raw asymmetries for \BpDsDzb decays, split by \Dzb decay mode, data-taking year and magnet polarity.}
  \begin{center}
  \begin{tabular}{lclccc}
  \toprule
  Year    & Magnet polarity  & \Dzb        & $N(\Bm)$      & $N(\Bp)$      & $A_{\rm raw} (\%)$    \\
  \midrule
  2011    & \MagUp           & \DKpi      & $\phantom{0} 1649\pm\phantom{0} 45$ & $\phantom{0} 1704\pm\phantom{0} 46$ & $           -1.6\pm1.9$ \\
          &                  & \DKpipipi  & $\phantom{00} 957\pm\phantom{0} 35$ & $\phantom{00} 978\pm\phantom{0} 37$ & $          -1.1\pm2.6$ \\
          &                  & combined   & $\phantom{0} 2606\pm\phantom{0} 57$ & $\phantom{0} 2682\pm\phantom{0} 59$ & $          -1.4\pm1.6$ \\
  \midrule                                                                        
  2011    & \MagDown         & \DKpi      & $\phantom{0} 2376\pm\phantom{0} 54$ & $\phantom{0} 2523\pm\phantom{0} 55$ & $          -3.0\pm1.6$ \\
          &                  & \DKpipipi  & $\phantom{0} 1321\pm\phantom{0} 42$ & $\phantom{0} 1304\pm\phantom{0} 44$ & $\phantom{-} 0.6\pm2.3$ \\
          &                  & combined   & $\phantom{0} 3696\pm\phantom{0} 68$ & $\phantom{0} 3828\pm\phantom{0} 70$ & $           -1.7\pm1.3$ \\
  \midrule                                                                        
  2011    & combined         & \DKpi      & $\phantom{0} 4024\pm\phantom{0} 71$ & $\phantom{0} 4231\pm\phantom{0} 72$ & $          -2.5\pm1.2$ \\
          &                  & \DKpipipi  & $\phantom{0} 2286\pm\phantom{0} 55$ & $\phantom{0} 2283\pm\phantom{0} 57$ & $\phantom{-} 0.1\pm1.7$ \\
          &                  & combined   & $\phantom{0} 6310\pm\phantom{0} 90$ & $\phantom{0} 6513\pm\phantom{0} 92$ & $           -1.6\pm1.0$ \\
  \midrule                                                                        
  2012    & \MagUp           & \DKpi      & $\phantom{0} 4781\pm\phantom{0} 77$ & $\phantom{0} 5073\pm\phantom{0} 79$ & $           -3.0\pm1.1$ \\
          &                  & \DKpipipi  & $\phantom{0} 2738\pm\phantom{0} 62$ & $\phantom{0} 2958\pm\phantom{0} 64$ & $           -3.9\pm1.6$ \\
          &                  & combined   & $\phantom{0} 7519\pm\phantom{0} 99$ & $\phantom{0} 8031\pm           101$ & $           -3.3\pm0.9$ \\
  \midrule                                                                        
  2012    & \MagDown         & \DKpi      & $\phantom{0} 4860\pm\phantom{0} 77$ & $\phantom{0} 4870\pm\phantom{0} 78$ & $           -0.1\pm1.1$ \\
          &                  & \DKpipipi  & $\phantom{0} 2696\pm\phantom{0} 61$ & $\phantom{0} 2711\pm\phantom{0} 59$ & $           -0.3\pm1.6$ \\
          &                  & combined   & $\phantom{0} 7556\pm\phantom{0} 98$ & $\phantom{0} 7581\pm\phantom{0} 98$ & $           -0.2\pm0.9$ \\
  \midrule                                                                        
  2012    & combined         & \DKpi      & $\phantom{0} 9641\pm           108$ & $\phantom{0} 9979\pm           111$ & $           -1.7\pm0.8$ \\
          &                  & \DKpipipi  & $\phantom{0} 5430\pm\phantom{0} 87$ & $\phantom{0} 5660\pm\phantom{0} 87$ & $           -2.1\pm1.1$ \\
          &                  & combined   & $           15071\pm           139$ & $           15639\pm           141$ & $           -1.8\pm0.6$ \\
  \midrule                                                                        
  2011/12 & \MagUp           & \DKpi      & $\phantom{0} 6430\pm\phantom{0} 89$ & $\phantom{0} 6777\pm\phantom{0} 91$ & $           -2.6\pm1.0$ \\
          &                  & \DKpipipi  & $\phantom{0} 3702\pm\phantom{0} 71$ & $\phantom{0} 3943\pm\phantom{0} 74$ & $           -3.1\pm1.3$ \\
          &                  & combined   & $           10132\pm           114$ & $           10720\pm           117$ & $           -2.8\pm0.8$ \\
  \midrule                                                                        
  2011/12 & \MagDown         & \DKpi      & $\phantom{0} 7227\pm\phantom{0} 94$ & $\phantom{0} 7432\pm           102$ & $           -1.4\pm0.9$ \\
          &                  & \DKpipipi  & $\phantom{0} 4019\pm\phantom{0} 74$ & $\phantom{0} 4012\pm\phantom{0} 73$ & $\phantom{-} 0.1\pm1.3$ \\
          &                  & combined   & $           11246\pm           119$ & $           11444\pm           125$ & $          -0.9\pm0.8$ \\
  \midrule                                                                        
  2011/12 & combined         & \DKpi      & $           13659\pm           129$ & $           14209\pm           132$ & $           -2.0\pm0.7$ \\
          &                  & \DKpipipi  & $\phantom{0} 7717\pm           103$ & $\phantom{0} 7945\pm           104$ & $           -1.5\pm0.9$ \\
          &                  & combined   & $           21375\pm           165$ & $           22153\pm           168$ & $           -1.8\pm0.5$ \\
  \bottomrule
  \end{tabular}
  \end{center}
\label{tab:Arawsplit}
\end{table}
\vspace{-0.4cm}

Four sources of detection asymmetry are considered: the trigger asymmetry (section~\ref{sec:triggerasym}); the tracking asymmetry (section~\ref{sec:trackingasym}); the PID asymmetry (section~\ref{sec:pidasym}); and the kaon detection asymmetry (section~\ref{sec:kaonasym}). Since the asymmetries are small, the total detection asymmetry can be expressed as
\begin{equation}
	A_D = A_{\rm trigger} + A_{\rm tracking} + A_{\rm PID} + A_{D}(\Km\pip).
\end{equation}
The contribution of each component to the detection asymmetry is summarised in table~\ref{tab:detasym}.

\begin{table}[tbh]
  \caption{Detection asymmetries for \BpDporDsDzb decays. The total is the sum of the individual components, with their uncertainties added in quadrature.}
  \begin{center}\begin{tabular}{llccccc}
  \toprule
  Channel  & $A_{\rm trigger}(\%)$ & $A_{\rm tracking}(\%)$  & $A_{\rm PID}(\%)$ & $A_D(\Km\pip)(\%)$ & total (\%)\\
  \midrule
  \decay{\Bp}{\Ds\Dzb} & $0.0\pm 0.0$ & $0.2\pm 0.1$ & $0.0\pm 0.1$ & $-1.0\pm 0.2$ & $-0.8\pm 0.2$\\
  \decay{\Bp}{\Dp\Dzb} & $0.0\pm 0.0$ & $0.2\pm 0.1$ & $0.0\pm 0.1$ & $\phantom{-}0.0\pm 0.0$ & $\phantom{-}0.2\pm 0.1$\\
  \bottomrule
  \end{tabular}\end{center}
\label{tab:detasym}
\end{table}

\subsection{Trigger asymmetry}
\label{sec:triggerasym}

The trigger is made up of two stages: the L0 trigger, implemented in hardware; and the high level trigger, implemented in software (see section~\ref{sec:trigger}). From other studies of \CP violation~\cite{LHCb-PAPER-2018-010}, the hardware trigger is known to result in $\mathcal{O}(0.1\%)$ asymmetries, while the software trigger results in $\mathcal{O}(0.01\%)$ asymmetries, which are considered negligible compared to the statistical uncertainties. At L0, the $B$ candidates are required to be either TIS on the global trigger or TOS on the hadron trigger. The trigger selection is described in full in section~\ref{sec:triggersel}.

\newpage
\paragraph{L0 global TIS}\mbox{}\vspace{0.3cm}

\noindent The L0 global TIS asymmetry has been measured with \decay{\B}{\Dzb\mup\neum X} decays using Run I data collected by the \lhcb experiment~\cite{LHCb-PAPER-2016-054}. In a \decay{\B}{\Dzb\mup\neum X} event, one of the $b$ quarks from the \bbbar pair will hadronise into a \B meson and decay via \decay{\B}{\Dzb\mup\neum X}. The rest of the event, including the presence of the $b$ quark that doesn't decay via the signal channel, is identical between \decay{\Bp}{\DporDs\Dzb} and \decay{\B}{\Dzb\mup\neum X}. Hence, the L0 global TIS asymmetry is also valid for \decay{\Bp}{\DporDs\Dzb}. The L0 global TIS asymmetry multiplied by the TIS fraction of the signal ($\sim 60\%$) results in an asymmetry of $0.04\%$ and is considered negligible.

\paragraph{L0 hadron TOS}\mbox{}\vspace{0.3cm}

\noindent If the \hcal has a non-uniform response, the TOS requirement on the L0 hadron trigger can result in an asymmetry. The performance of the hadron trigger is not well reproduced in simulation, so the efficiency of the TOS requirement is determined from data using a \DKpi calibration sample~\cite{MartinSanchez:1407893}. The efficiency is given by
\begin{equation}
	\varepsilon_{\rm trigger} = \frac{N({\rm TIS \ and \ TOS})}{N({\rm TIS})},
\end{equation}
where $N({\rm TIS \ and \ TOS})$ is the number of candidates passing both the L0 global TIS and L0 hadron TOS requirements and $N({\rm TIS})$ is the number passing only L0 global TIS. The asymmetry is calculated separately for $h=K,\pi$ with
\begin{equation}
	A_{\rm trigger}(h) = \frac{\varepsilon(h^-)-\varepsilon(h^+)}{\varepsilon(h^-)+\varepsilon(h^+)},
\end{equation}
and is shown in figure~\ref{fig:L0trigasym} as a function of \pt. At low \pt, there are large negative asymmetries of up to $-5\%$ and at high \pt, there are small $(<1\%)$ asymmetries. 

\begin{figure}[tb]
  \subfloat[][Kaon]{\includegraphics[width=0.48\textwidth]{L0Hadron_2012_MagnetBoth_Kaon_asymmetry}\label{fig:L0K}}\quad
  \subfloat[][Pion]{\includegraphics[width=0.48\textwidth]{L0Hadron_2012_MagnetBoth_Pi_asymmetry}\label{fig:L0pi}}\quad\\
  \caption{L0 hadron TOS trigger asymmetry as a function of \pt in data collected in 2012 with both magnet polarities combined~\cite{MartinSanchez:1407893}.}
  \label{fig:L0trigasym}
\end{figure}

The asymmetries for the individual hadrons need to be folded with the \pt distributions of the final state particles from \BpDporDsDzb decays and combined into an overall asymmetry for the \Bp meson. In data, only events that pass the L0 trigger requirements are saved, so generator level (without simulation of the detector response) MC was used to model the \pt spectra of the tracks without any trigger requirements. A total of $70,000$ events are generated and momentum requirements from the stripping and preselection are applied. The efficiencies of the final state particles are combined using the logical OR,
\begin{equation}
	\varepsilon(B) = 1-\prod^{N_{\rm tracks}}_{i=1} (1-\varepsilon(h_i)),
\end{equation}
since the event is saved if any of the tracks fire the trigger. It is assumed that the $\varepsilon(h_i)$ are independent. For each event, the efficiency is calculated assuming the $B$ meson was both positively and negatively charged to give an event-by-event asymmetry,
\begin{equation}
	\label{eq:trigasym}
	A_{\rm trigger}(B) = \frac{\varepsilon(\Bm)-\varepsilon(\Bp)}{\varepsilon(\Bm)+\varepsilon(\Bp)}.
\end{equation}
This is weighted with the average efficiency, $(\varepsilon(\Bm)+\varepsilon(\Bp))/2$, since events with a higher efficiency are more likely to pass the trigger selection and contribute to the final asymmetry.

Figure~\ref{fig:ebeL0trigasym} shows an example of the weighted event-by-event asymmetry, where the mean gives the size of the L0 hadron TOS asymmetry. Averaging by data-taking year, magnet polarity, \Dzb channel and multiplying by the TOS fraction of the signal ($64\%$ for \DKpi and $61\%$ for \DKpipipi) results in an asymmetry of $-0.045\%$ for \BpDsDzb and $0.030\%$ for \BpDpDzb. The uncertainty on the mean is $\mathcal{O}(0.001\%)$ and is considered negligible. 

\begin{figure}[tb]
  \subfloat[][\BpDsDzb, \DKpi]{\includegraphics[width=0.48\textwidth]{hist_BpDsDzKpi_2012_MagnetBoth_asym_Bp_cuts}\label{fig:asymDs}}\quad
  \subfloat[][\BpDpDzb, \DKpi]{\includegraphics[width=0.48\textwidth]{hist_BpDpDzKpi_2012_MagnetBoth_asym_Bp_cuts}\label{fig:asymDp}}\quad\\
  \caption{Event-by-event L0 hadron TOS asymmetry, weighted by the average efficiency, in data collected in 2012 with both magnet polarities combined.}
  \label{fig:ebeL0trigasym}
\end{figure}

Only momentum cuts from the preselection and stripping are applied to the generator level MC. The track \pt distributions are used as input to the MVA, as well as other slightly correlated variables; hence there may be differences between the \pt distributions in MC and data. To estimate the size of this effect, the asymmetry is re-evaluated without applying any momentum cuts. The resulting change is less than 0.03\%.

The efficiencies calculated using the \DKpi calibration sample have an associated uncertainty. To take this into account, the calculation is repeated varying the efficiencies according to a Gaussian distribution of width the value of the uncertainty. Figure~\ref{fig:toysL0trigasym} shows an example of the distribution of the asymmetry recalculated in this way 500 times. The root mean square (RMS) of the distributions in all channels and data-taking years is 0.03\% or less. As the size of the L0 hadron TOS asymmetry and its uncertainties are below $0.05\%$, no correction is applied to the \CP asymmetry measurement.

\begin{figure}[tb]
  \subfloat[][\BpDsDzb, \DKpi]{\includegraphics[width=0.48\textwidth]{hist_BpDsDzKpi_2012_MagnetBoth_asym_toys_Bp_cuts}\label{fig:toysasymDs}}\quad
  \subfloat[][\BpDpDzb, \DKpi]{\includegraphics[width=0.48\textwidth]{hist_BpDpDzKpi_2012_MagnetBoth_asym_toys_Bp_cuts}\label{fig:toysasymDp}}\quad\\
  \caption{L0 hadron TOS asymmetry, calculated by varying the efficiencies according to Gaussian distributions, using data collected in 2012 with both magnet polarities combined.}
  \label{fig:toysL0trigasym}
\end{figure}

\subsection{Tracking asymmetry}
\label{sec:trackingasym}

The tracking efficiency has been studied in Run I data using pions from \decay{\Dstarp}{\pip(\decay{\Dz}{\Km\pip\pim\pip})} decays~\cite{LHCb-PAPER-2016-013}. The efficiency was determined as a function of momentum by comparing the partially reconstructed \Dstarp yields with the fully reconstructed yields when a pion is added,
\begin{equation}
\varepsilon_{\rm tracking}(\pi)=\frac{N_{\rm full}(\pi)}{N_{\rm partial}(\pi)}.
\end{equation}
Figure~\ref{fig:pitrackasym} shows the asymmetry derived from these efficiencies. Although each individual magnet polarity shows a strong dependence on momentum, a large cancellation is expected from their combination.

\begin{figure}[b]
\includegraphics[width=0.48\textwidth]{pion_tracking_asymmetry_2012_lowpbin}
\caption{Pion tracking asymmetry in data collected in 2012~\cite{LHCb-PAPER-2016-013}.}
\label{fig:pitrackasym}
\end{figure}

A total of $70,000$ generator level MC events are used to model the momentum distributions of the \BpDporDsDzb tracks. Momentum requirements from the stripping and preselection are applied. As the tracking efficiency applies to all final state tracks, the efficiencies are combined using the logical AND,
\begin{equation}
	\varepsilon(B)=\prod_{i=1}^{N_{\rm tracks}} \varepsilon(h_i),
\end{equation}
where the kaons in the final state were treated as pions. As for the L0 hadron TOS asymmetry (see section~\ref{sec:triggerasym}), an event-by-event asymmetry is calculated assuming the $B$ meson is both positively and negatively charged, then weighted with the average efficiency. 

Figure~\ref{fig:ebetrackasym} shows an example of the distribution of the weighted event-by-event asymmetry, where the mean gives the size of the tracking asymmetry. As there are fewer bins in the measurement of the tracking asymmetry than in the L0 hadron TOS asymmetry, the distribution of the event-by-event asymmetry is not smoothly-varying. Averaging by data-taking year, magnet polarity and \Dzb channel results in an asymmetry of $+0.18\%$ for \BpDsDzb and $+0.21\%$ for \BpDpDzb. The uncertainty on the mean is $\mathcal{O}(0.01\%)$ and is considered negligible. 

\begin{figure}[tb]
  \subfloat[][\BpDsDzb, \DKpi]{\includegraphics[width=0.48\textwidth]{hist_BpDsDzKpi_2012_up_asym_Bp_cuts_lowpbin}\label{fig:trackasymDs}}\quad
  \subfloat[][\BpDpDzb, \DKpi]{\includegraphics[width=0.48\textwidth]{hist_BpDpDzKpi_2012_up_asym_Bp_cuts_lowpbin}\label{fig:trackasymDp}}\quad\\
  \caption{Event-by-event tracking asymmetry, weighted by the average efficiency, in data collected in 2012 with the \MagUp polarity.}
  \label{fig:ebetrackasym}
\end{figure}

The same sources of systematic uncertainty as for the L0 hadron TOS asymmetry are considered. To evaluate the effect of only applying momentum requirements from the preselection and stripping, the calculation is repeated without any requirements. The resulting change in the asymmetry is less than $0.03\%$. To take into account the uncertainties on the efficiencies, the calculation is repeated varying the efficiencies according to Gaussian distributions 500 times. Figure~\ref{fig:toystrackasym} shows the distribution of the asymmetry. 
%The values for the efficiencies are shared between \Dzb channels, but independent for each magnet polarity and data-taking year. As the distributions of the asymmetries are filled with the combination of \Dzb channels, this correlation is taken into account. 
This results in an uncertainty of 0.07\% for both \BpDsDzb and \BpDpDzb decays and is the dominant uncertainty on the tracking asymmetry.

\begin{figure}[tb]
  \subfloat[][\BpDsDzb]{\includegraphics[width=0.48\textwidth]{hist_BpDsDz_combined_combined_combined_asym_toys_Bp_cuts_lowpbin}\label{fig:toystrackasymDs}}\quad
  \subfloat[][\BpDpDzb]{\includegraphics[width=0.48\textwidth]{hist_BpDpDz_combined_combined_combined_asym_toys_Bp_cuts_lowpbin}\label{fig:toystrackasymDp}}\quad\\
  \caption{Tracking asymmetry, calculated by varying the efficiencies according to Gaussian distributions, with data-taking years, magnet polarities and \Dzb channels combined.}
  \label{fig:toystrackasym}
\end{figure}

\subsection{PID asymmetry}
\label{sec:pidasym}

%If there is a large detection asymmetry, this is expected to be evident in the high-statistics \BpDsDzb channel. However, this channel differs from \BpDpDzb by the kaon and pion content of the final state. Therefore, the detection asymmetry due to the PID requirements was evaluated separately. 
%PID requirements were placed on the final state particles at all stages of the event selection: the trigger, stripping, preselection and MVA.

The PID information, which is based on the response of the \rich detectors, is dependent on magnet polarity and track charge. The efficiencies of a number of PID requirements were studied using samples of \DKpi decays, selected using only kinematical and topological information, as a function of $p$, $\eta$ and the number of tracks (nTracks) in the event~\cite{Anderlini:2202412}. By folding these efficiencies with the distributions of $p$, $\eta$ and nTracks from \BpDporDsDzb simulation without any PID requirements applied, the efficiency of any given \dllkpi requirement can be determined. The efficiency of the requirements in the preselection ($\dllkpi(K)>-5$ and $\dllkpi(\pi)<10$) is $\sim 95\%$. The \dllkpi distributions are used as input to the MVA, so the precise requirements placed on them are unknown. However, as the efficiency of the MVA requirement is $\sim 90\%$, the minimum PID efficiency is the product of the two, $95\%\times 90\% = \sim 85\%$.

Table~\ref{tab:PIDeff} shows the efficiencies of $\dllkpi(K)>x$ and $\dllkpi(\pi)<-x$ requirements for $x=0,10,20$, on all final state particles from \BpDporDsDzb decays. After combining magnet polarities, the asymmetry induced by the $x=0$ requirement is $0.11\%$ or less for both channels and data-taking years. Since the efficiency of this requirement is lower than the minimum PID efficiency, the asymmetry is expected to be smaller than $0.11\%$. Therefore, no correction is applied to the \CP asymmetry measurement and a conservative systematic uncertainty of $0.1\%$ is assigned.

It could be argued that the loss of signal efficiency from applying the PID requirements is due to a small number of events experiencing a requirement tighter than $x = 0$, \ie $x = 20$. Table~\ref{tab:PIDeff} shows that, after combining magnet polarities, the PID asymmetry for the $x = 20$ requirement, is 0.35\% or less for both channels and data-taking years. As the minimum PID efficiency is $\sim 85\%$ and the efficiency of the $x=20$ requirement is extremely low, at most 15\% of events can experience this requirement. The asymmetry is therefore diluted to $15\% \times 35\% \sim 0.05\%$, which is well covered by the assigned 0.1\% systematic uncertainty.
%The remaining 85\% of the events dilute the detector asymmetry induced by the $x = 20$ requirement to $15\% \times 35\% \sim 0.05\%$, which is well covered by the assigned 0.1\% systematic uncertainty.

\begin{sidewaystable}[p]
    %\begin{table}[tb]
      \caption{Efficiency of \dllkpi$(K)>x$ and \dllkpi$(\pi)<-x$ cuts for the \decay{\Bp}{\DporDs\Dzb} final states with \DKpi decays, split by charge and magnet polarity.
      The combination of magnet up and magnet down takes into account that approximately 43\% of events in 2011 were recorded with the magnet up polarity, whereas in 2012 the split is approximately even.}
      \begin{center}\begin{tabular}{llccccccccc}
      \toprule
      $\varepsilon_{\rm PID}$ (\%) & &                & $x=0$            &       &                & $x=10$           &       &                & $x=20$           &       \\
                                   & & \MagUp & \MagDown & comb. & \MagUp & \MagDown & comb. & \MagUp & \MagDown & comb. \\
      \midrule
      2011 & \decay{\Bp}{\Dp\Dzb}    & 62.09          & 62.23            & 62.17 & 14.89          & 14.93            & 14.91 & 2.199           & 2.225           &  2.214 \\
           & \decay{\Bm}{\Dm\Dz}     & 62.38          & 62.16            & 62.25 & 14.92          & 14.86            & 14.89 & 2.210           & 2.225           &  2.219 \\\cmidrule{3-11}
           & $A_{\rm PID}$ (\%)               & 0.23           & -0.06            & 0.06  & 0.10           & -0.23            & -0.07 & 0.249           & 0.000           &  0.113 \\\cmidrule{3-11}
           & \decay{\Bp}{\Ds\Dzb}    & 67.26          & 67.34            & 67.31 & 17.39          & 17.79            & 17.62 & 2.341           & 2.440           &  2.397 \\
           & \decay{\Bm}{\Dsm\Dz}    & 67.42          & 67.49            & 67.46 & 17.50          & 17.66            & 17.59 & 2.366           & 2.414           &  2.393 \\\cmidrule{3-11}
           & $A_{\rm PID}$ (\%)               & 0.12           &  0.11            & 0.11  & 0.32           & -0.37            & -0.09 & 0.531           &-0.536           & -0.084 \\
      \midrule
      2012 & \decay{\Bp}{\Dp\Dzb}    & 65.13          & 66.59            & 65.86 & 15.87          & 17.11            & 16.49 & 2.382           & 2.700           & 2.541  \\
           & \decay{\Bm}{\Dm\Dz}     & 65.34          & 66.52            & 65.93 & 16.04          & 17.08            & 16.56 & 2.420           & 2.698           & 2.559  \\\cmidrule{3-11}
           & $A_{\rm PID}$ (\%)               & 0.16           & -0.05            & 0.05  & 0.53           & -0.09            &  0.21 & 0.791           &-0.037           & 0.353  \\\cmidrule{3-11}
           & \decay{\Bp}{\Ds\Dzb}    & 69.50          & 70.79            & 70.15 & 18.24          & 19.64            & 18.94 & 2.497           & 2.832           & 2.665  \\
           & \decay{\Bm}{\Dsm\Dz}    & 69.65          & 70.42            & 70.04 & 18.38          & 19.48            & 18.93 & 2.525           & 2.777           & 2.651  \\\cmidrule{3-11}
           & $A_{\rm PID}$ (\%)               & 0.11           & -0.26            & -0.08 & 0.38           & -0.41            & -0.03 & 0.558           &-0.981           &-0.263  \\
      \bottomrule
      \end{tabular}\end{center}
    \label{tab:PIDeff}
    %\end{table}
\end{sidewaystable}
\vspace{-0.4cm}
\setcounter{page}{145}


\subsection{Kaon detection asymmetry}
\label{sec:kaonasym}

Positively charged kaons are composed of $u$ and $\bar{s}$ quarks, whereas negatively charged kaons are composed of $s$ and $\bar{u}$ quarks. As the \lhcb detector largely consists of $u$ and $d$ quarks in the form of protons and neutrons, \Km mesons interact more strongly with the detector material than \Kp mesons due to $u\bar{u}$ annihilation. This effect is momentum dependent since, as the momentum of the kaon increases, the sea quarks become more likely to interact with the detector material than the valence quarks, and the detection asymmetry decreases.

The final states of \BpDpDzb and \BpDsDzb decays are $\Km\pip\pip\Kp\pim(\pip\pim)$ and $\Kp\Km\pip\Kp\pim(\pip\pim)$, respectively. As there are two oppositely charged kaons in the final state of the \BpDpDzb decay, the kaon detection asymmetry is expected to largely cancel. However, as there are an odd number of kaons in the final state of the \BpDsDzb decay, there is no such cancellation and an asymmetry is expected.

The difference in detection asymmetry between kaons and pions, $A_D(\Km\pip)$, has been measured as a function of kaon momentum by comparing the yield of \decay{\Dp}{\Km\pip\pim} decays with the yield of \decay{\Dp}{\Kzb\pip} decays~\cite{LHCb-PAPER-2014-013}. The raw asymmetry in \decay{\Dp}{\Km\pip\pip} decays is,
\begin{equation}
	A_{\rm raw}(\Km\pip\pip) = A_P(\Dp) + A_D(\Km\pip) + A_D(\pip),
\end{equation}
where $A_P(\Dp)$ is the \Dp production asymmetry and $A_D(\pip)$ is the pion detection asymmetry. The raw asymmetry in \decay{\Dp}{\Kzb\pip} decays is,
\begin{equation}
	A_{\rm raw}(\Kzb\pip) = A_P(\Dp) +  A_D(\pip) - A_D(\Kz),
\end{equation}
where $A_D(\Kz)$ is the detection asymmetry of \decay{\Kz}{\pip\pim} decays. The measurement of $A_D(\Kz)$ takes into account \CP violation, $\Kz-\Kzb$ mixing and the difference in the interaction cross-section of \Kz and \Kzb with the detector material. As \decay{\Dp}{\Km\pip\pip} and \decay{\Dp}{\Kzb\pip} decays are Cabibbo favoured, it is assumed there is negligible \CP violation. Hence, $A_D(\Km\pip)$ is obtained with,
\begin{equation}
	A_D(\Km\pip) = A_{\rm raw}(\Km\pip\pip) - A_{\rm raw}(\Kzb\pip) - A_D(\Kz).
\end{equation}

Table~\ref{tab:kaondet} shows the asymmetry calculated by folding the measurement of $A_D(\Km\pip)$ with the momentum spectra of kaons from simulated \BpDporDsDzb decays. The uncertainties on the measurement of $A_D(\Km\pip)$ are taken into account by repeating the calculation with the values of $A_D(\Km\pip)$ shifted up or down by $1\sigma$. As expected, $A_D(\Km\pip)$ in \BpDpDzb decays is consistent with zero, whereas a significantly non-zero value of $(-1.0\pm 0.2)\%$ is measured for \BpDsDzb decays. As the kaons are treated as pions in the evaluation of the tracking asymmetry, the sum of $A_D(\Km\pip)$ and $A_{\rm tracking}$ isolates the kaon detection asymmetry.

\begin{table}[tb]
  \caption{Difference in kaon and pion detection asymmetry for \BpDporDsDzb decays. The values quoted are for the underlined particle. The asymmetry of the $B$ meson is the sum of the individual kaon asymmetries, multiplied by their charge.}
  \begin{center}\begin{tabular}{lcc}
  \toprule
Particle                                  & $\langle p \rangle$ (\gevc) & $A_D(\Km\pip)$ (\%) \\
  \midrule
\decay{\Bp}{\Ds(\underline{\Kp}\Km\pip)\Dzb(\Kp\pim)}          & 25.6        & $-1.10\pm0.14$ \\
\decay{\Bp}{\Ds(\Kp\underline{\Km}\pip)\Dzb(\Kp\pim)}          & 25.8        & $-1.10\pm0.14$ \\
\decay{\Bp}{\Ds(\Kp\Km\pip)\Dzb(\underline{\Kp}\pim)}          & 32.6        & $-1.03\pm0.16$ \\
\decay{\underline{\Bp}}{\Ds(\Kp\Km\pip)\Dzb(\Kp\pim)}          &             & $-1.03\pm0.16$ \\
\midrule
\decay{\Bp}{\Ds(\underline{\Kp}\Km\pip)\Dzb(\Kp\pim\pip\pim)}  & 27.5        & $-1.07\pm0.15$ \\
\decay{\Bp}{\Ds(\Kp\underline{\Km}\pip)\Dzb(\Kp\pim\pip\pim)}  & 28.0        & $-1.07\pm0.15$ \\
\decay{\Bp}{\Ds(\Kp\Km\pip)\Dzb(\underline{\Kp}\pim\pip\pim)}  & 30.2        & $-1.05\pm0.15$ \\
\decay{\underline{\Bp}}{\Ds(\Kp\Km\pip)\Dzb(\Kp\pim\pip\pim)}  &             & $-1.05\pm0.15$ \\
\midrule
\decay{\Bp}{\Ds\Dzb}, \Dzb combined                             &             & $-1.04\pm0.16$ \\
\midrule
\decay{\Bp}{\Dp(\underline{\Km}\pip\pip)\Dzb(\Kp\pim)}         & 28.2        & $-1.06\pm0.15$ \\
\decay{\Bp}{\Dp(\Km\pip\pip)\Dzb(\underline{\Kp}\pim)}         & 32.1        & $-1.02\pm0.16$ \\
\decay{\underline{\Bp}}{\Dp(\Km\pip\pip)\Dzb(\Kp\pim)}         &             & $\phantom{-}0.04\pm0.01$  \\
\midrule
\decay{\Bp}{\Dp(\underline{\Km}\pip\pip)\Dzb(\Kp\pim\pip\pim)} & 30.4        & $-1.03\pm0.16$ \\
\decay{\Bp}{\Dp(\Km\pip\pip)\Dzb(\underline{\Kp}\pim\pip\pim)} & 29.9        & $-1.03\pm0.15$ \\
\decay{\underline{\Bp}}{\Dp(\Km\pip\pip)\Dzb(\Kp\pim\pip\pim)} &             & $\phantom{-}0.00\pm0.00$ \\
\midrule
\decay{\Bp}{\Dp\Dzb}, \Dzb combined                             &             & $\phantom{-}0.02\pm0.01$ \\
  \bottomrule
  \end{tabular}\end{center}
\label{tab:kaondet}
\end{table}
\vspace{-0.4cm}

\section{Production asymmetry}
\label{sec:productionasym}

The content of \Bp mesons is $u$ and $\bar{b}$ quarks, whereas \Bm mesons consist of $b$ and $\bar{u}$ quarks. As the \lhc is a $pp$, or $uuduud$, collider, the production of \Bp mesons is favoured over \Bm mesons. Hence, from equation~\ref{eq:prodasym}, a negative production asymmetry is expected. The raw asymmetry needs to be corrected for this effect in order to determine the \CP asymmetry.

The \Bp production asymmetry has been measured in Run I data using \decay{\Bp}{\Dzb\pip} decays~\cite{LHCb-PAPER-2016-054}. The raw asymmetry is related to the production asymmetry as follows,
\begin{equation}
	A_{\rm raw}(\decay{\Bp}{\Dzb\pip}) = A_P(\Bp) + A_D(\Dzb\pip) + \mathcal{A}^{\CP}(\decay{\Bp}{\Dzb\pip}),
\end{equation}
where $A_D(\Dzb\pip)$ is the detection asymmetry of the $\Dzb\pip$ final state and $\mathcal{A}^{\CP}(\decay{\Bp}{\Dzb\pip})$ is the \CP asymmetry. The \CP asymmetry was estimated from measurements of the CKM angle $\gamma$ and the hadronic parameters of \decay{\Bp}{\Dzb\pip} decays. Averaging the production asymmetries measured at 7 and 8\tev results in a value of $A_P(\Bp)=(-0.5\pm0.4)\%$. In the measurement of $A_P(\Bp)$, no significant dependence on the production spectrum ($\pt, y$) was observed.

\section{Results}
\label{sec:results}

The \CP asymmetry is determined by subtracting the production (see section~\ref{sec:productionasym}) and detection asymmetries (see table~\ref{tab:detasym}) from the raw asymmetry, according to equation~\ref{eq:asymrel}. The results are
\begin{align}
	\mathcal{A}^{\CP}(\BpDsDzb) &= (-0.4\pm 0.5\pm 0.5)\%,\\
	\mathcal{A}^{\CP}(\BpDpDzb) &= (+2.3\pm 2.7\pm 0.4)\%,
\end{align}
where the first uncertainty is statistical and the second systematic. 
%The systematic uncertainties are summarised in table~\ref{tab:ACPsystematics}, with 
The dominant source of systematic uncertainty originates from the production asymmetry. As the production asymmetry is only dependent on the flavour of the $B$ meson and not the decay mode, it cancels in the difference of the two measurements,
\begin{equation}
	\mathcal{A}^{\CP}(\BpDsDzb) - \mathcal{A}^{\CP}(\BpDpDzb) = (-2.7\pm 2.8\pm 0.2)\%.
\end{equation}

This is the first ever measurement of $\mathcal{A}^{\CP}(\BpDsDzb)$ and the measurement of $\mathcal{A}^{\CP}(\BpDpDzb)$ is the most precise to date, improving upon the precision of the previous world-average value, $(-3\pm 7)\%$~\cite{PDG2018}, by a factor of $2.6$. The results are consistent with zero and hence there is no evidence of \CP violation. The measurements of $\mathcal{A}^{\CP}(\BpDpDzb)$ and $\mathcal{A}^{\CP}(\BpDsDzb)$ place constraints on the range of allowed values predicted by $R$-parity violating supersymmetry~\cite{Kim:2008ex} and the fourth generation model~\cite{Xu2016}, respectively. 
	
%\begin{table}[tb]
%  \caption{Systematic uncertainties, in \%, on the \ACP measurement.
%  The total is calculated as the quadratic sum of the individual components.}
%  \begin{center}\begin{tabular}{llccccc}
%  \toprule
%  Channel  & $A_P$ & $A_K$ & $A_{\rm tracking}$  & $A_{\rm PID}$ & total\\
%  \midrule
%  \decay{\Bp}{\Ds\Dzb} & 0.4 & 0.2 & 0.1 & 0.1 & 0.5\\
%  \decay{\Bp}{\Dp\Dzb} & 0.4 & 0.0 & 0.1 & 0.1 & 0.4\\
%  \bottomrule
%  \end{tabular}\end{center}
%\label{tab:ACPsystematics}
%\end{table}