%% Title
\titlepage[\textit{of}\\ Peterhouse\\ \vspace{0.3cm} University of Cambridge]{%
 
  \vspace{-6.5cm}

  \begin{center}
%  	\includegraphics[height=4cm]{peterhouse_coat_of_arms}
%  	\hspace{1cm}
  	\includegraphics[height=4cm]{cambridge_uni_coat_of_arms}
  \end{center}
  
  \vspace{1.25cm}
	
  This dissertation is submitted to the University of Cambridge\\ for the degree of Doctor of Philosophy\\ \vspace{0.3cm}
  June 2019  
  }

%% Abstract
\begin{abstract}%[\smaller \thetitle\\ \vspace*{1cm} \smaller {\theauthor}]
%\thispagestyle{empty}
\CP violation is a necessary ingredient to create a matter-antimatter asymmetry. However, the amount of \CP violation incorporated in the Standard Model is orders of magnitude too small to explain the observed asymmetry. This thesis documents two studies of \CP violation with doubly charmed $B$ decays. Both were performed using Run I data collected by the \lhcb experiment in $pp$ collisions at the \lhc. This corresponds to $1.0\invfb$ collected at a centre-of-mass energy of $7\tev$ and $2.0\invfb$ collected at a centre-of-mass energy of $8\tev$. 
  
The first analysis is a search for \Bc decays to two charm mesons, \decay{\Bc}{\DporDsorstar\DorDstar}, where the second charm meson is either \Dz or \Dzb, which are sensitive to the CKM angle \gamma. No evidence for signal is found and limits are set on twelve decay modes. The second analysis is the measurement of the \CP asymmetry in \BpDporDsDzb decays. The results are
\begin{align*}
	\mathcal{A}^{\CP}(\BpDsDzb) &= (-0.4\pm 0.5\pm 0.5)\%,\\
	\mathcal{A}^{\CP}(\BpDpDzb) &= (+2.3\pm 2.7\pm 0.4)\%,
\end{align*}
where the first uncertainty is statistical and the second systematic. This is the first measurement of $\mathcal{A}^{\CP}(\BpDsDzb)$ and the most precise measurement of $\mathcal{A}^{\CP}(\BpDpDzb)$ to date. The results are consistent with \CP symmetry.
\end{abstract}


%% Declaration
\begin{declaration}
  \noindent This dissertation is the result of my own work, except where explicit
  reference is made to the work of others, and has not been submitted
  for another qualification to this or any other university. This
  dissertation does not exceed the word limit for the Degree
  Committee of the Faculty of Physics \& Chemistry.
  \vspace*{1cm}
  \begin{flushright}
    Alison Maria Tully
  \end{flushright}
\end{declaration}


%% Acknowledgements
\begin{acknowledgements}
\noindent Completing a PhD, particularly as a member of a large collaboration such as \lhcb, is never just the work of one, but uncountably many. First, I would like to thank my supervisor, Val Gibson, for giving me the opportunity to study at Cambridge University and for providing me with numerous helpful suggestions throughout my PhD. I owe a huge debt of gratitude to Rolf Oldeman, without whose patient guidance and seemingly endless knowledge of particle physics the work in this thesis would not have been completed. 

The Cambridge \lhcb group has been a stimulating environment to work in for the past three-and-a-half years. The feedback provided during group meetings improved the results presented in this thesis immeasurably. I am particularly thankful to those I had the pleasure of working with directly: Matt Kenzie, Blaise Delaney, Ana Tri\v{s}ovi\'{c}, Susan Haines, and Chris Jones. The other members of the Cambridge HEP group, particularly those in the infamously-named WhatsApp group, made lunches, coffee breaks and formals most enjoyable. Special thanks to Will Fawcett for the brief stint we had as lifting partners and for voluntarily reading parts of this thesis.

Throughout my PhD, I have been a member of the $B$ to open charm physics working group within \lhcb. I'm grateful for the insightful questions asked on my talks in working group meetings and the helpful comments received on the documentation of the analyses. For two years, I oversaw the monstrous B2OC stripping code alongside Alessandro Bertolin. Thanks to Alessandro, this was (relatively) painless.

I had the good fortune to spend a year-and-a-half of my PhD based at \cern. The LTA community (quite literally) made it feel like a home away from home. Special thanks go to James Cowley, Mike Nelson, Amal Vaidya and Matt Tilley; your boundless enthusiasm for after-work beers in R1 was inspiring. I hope we continue to make Oxbridge pilgrimages for May Week until well after we're too old to be there.

I would like to thank the Science and Technology Facilities Council for not only funding my PhD, but also a brief interlude into the world of policy via a three month long fellowship at the Parliamentary Office of Science and Technology.\footnote{\href{https://researchbriefings.parliament.uk/ResearchBriefing/Summary/POST-PN-0584}{https://researchbriefings.parliament.uk/ResearchBriefing/Summary/POST-PN-0584}} Thanks also to the best POST supervisor, Lorna Christie, and fellow party cluster inhabitant, Rowena Bermingham, for enabling my caffeine addiction and inspiring a new-found love for bagels.

Thank you to my college, Peterhouse, for providing such a welcoming home for the two years I spent in Cambridge and for awarding me generous grants to tour the East Coast of the United States and attend the CERN Latin-American school of HEP in Argentina. Special thanks to my eclectic college squad (Jo Neville, Michael Waters, James Sinclair and Mohamed Derbal), for providing excellent dinner company and countless golden photography opportunities.

To my parents, Brenda and Richard, thank you for your support and encouragement over the years. Escaping to Glasgow during the holidays was always a welcome break. Thanks to the rest of my friends in Cambridge and London. To name just a few: Mel Singh, for always making me laugh and for introducing me to a passion of mine: climbing; Matteo Sbroscia, for the muffins and bike repairs; Mandy Choi, for the terrible complements and always providing a place to crash; and Jasmine Walter, for everything.
\end{acknowledgements}


%% Preface
\begin{preface}
  \noindent This thesis documents my contribution to finding an answer to one of the most fundamental questions in physics: \textit{why is there an asymmetry between matter and antimatter}? The key to this question could lie in the study of \CP violation. Doubly charmed $B$ decays provide a rich laboratory for the study of \CP violation due to the potential for large effects, both within and beyond the Standard Model. Two analyses of these decays are documented: the search for \Bc decays to two charm mesons and the measurement of the \CP asymmetry in \BpDporDsDzb decays. The results have been published in references~\cite{LHCb-PAPER-2017-045} and~\cite{LHCb-PAPER-2018-007}, respectively.

  A brief introduction to the Standard Model and its shortcomings is given in \textbf{\ChapterRef{chap:intro}}. \textbf{\ChapterRef{chap:theory}} describes the theoretical motivation for the study of doubly charmed $B$ decays, namely the sensitivity to the CKM angle \gamma that arises in the interference of \decay{\Bc}{\DporDsorstar\DzborDzbstar} and \decay{\Bc}{\DporDsorstar\DzorDzstar} decays and the new physics effects that can greatly enhance the \CP asymmetry in \BpDporDsDzb decays. The analyses are based on data collected by the \lhcb experiment, an overview of which is given in \textbf{\ChapterRef{chap:detector}}. The selection that has been developed to identify the most signal-like events is documented in \textbf{\ChapterRef{chap:selection}}. Most of the selection requirements are shared between \Bc and \Bp decays due to similarities between the two. \textbf{\ChapterRef{chap:Bc2DD}} describes the search performed for the \Bc decays and \textbf{\ChapterRef{chap:ACP}} the measurement of the \CP asymmetry in \BpDporDsDzb decays. Finally, the results are summarised in \textbf{\ChapterRef{chap:summary}}.
	  
  Throughout this dissertation charge conjugates of each decay mode are implied, except where explicitly stated. For example, \decay{\Bc}{\Ds\Dzb} refers to both \decay{\Bc}{\Ds\Dzb} and \decay{\Bcm}{\Dsm\Dz}.
  
\end{preface}

%% ToC
\tableofcontents


%% Strictly optional!
\frontquote{%
  A scientist in his laboratory is not a mere technician: \\he is also a child confronting natural phenomena that \\impress him as though they were fairy tales.}%
  {Marie Sk\l{}odowska-Curie}
%% I don't want a page number on the following blank page either.
\thispagestyle{empty}
