.PHONY: clean

thesis.pdf: thesis.tex preamble.tex intro/intro.tex theory/theory.tex detector/detector.tex selection/selection.tex Bc2DD/Bc2DD.tex ACP/ACP.tex summary/summary.tex frontmatter.tex appendices.tex
	@rm -f thesis.{aux,toc,lof,lot}
	(pdflatex thesis && bibtex thesis && pdflatex thesis && pdflatex thesis) || rm -f $(EXTRASTYS) thesis.pdf
	@rm -f thesis.{aux,toc,lof,lot}

clean:
	@rm -f thesis.pdf thesis.log thesis.aux
	@rm -f *.bbl *.blg *.lof *.cut
	@rm -f *.lot *.out *.toc
